#include <cstdlib>
#include "test-cases/case-1.h"
#include "test-cases/case-2.h"

int main() {
  system("clear");

  test::case_1();
  test::case_2();

  return 0;
}
