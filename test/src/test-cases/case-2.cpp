#include <memory>
#include <iostream>
#include <graph/graph.hpp>
#include <graph/node.hpp>
#include "case-2.h"

using namespace graph;

void test::case_2() {
  std::cout << "--> Graph testing(2) <--" << std::endl;

  std::unique_ptr<Graph<int>> graph =
    std::unique_ptr<Graph<int>>(Graph<int>::empty());

  std::cout << "Graph created" << std::endl;

  graph->link(Node<int>::shared_from_value(15));
  graph->link(Node<int>::shared_from_value(10));
  graph->link(Node<int>::shared_from_value(5));

  std::cout << "Nodes added (" << graph->count_nodes() << ")" << std::endl;
  std::cout << "Test first level connections\n" << std::endl;

  std::cout << "graph->0: " << graph->at(0)->get() << std::endl;
  std::cout << "graph->1: " << graph->at(1)->get() << std::endl;
  std::cout << "graph->2: " << graph->at(2)->get() << std::endl;

  graph->at(0)->link(graph->at(2));
  graph->at(1)->link(graph->at(0));
  graph->at(2)->link(graph->at(2));

  std::cout << "\nConnections added" << std::endl;
  std::cout << "--> Testing connections\n" << std::endl;

  std::cout << "graph->0->2: " << graph->at(0)->at(0)->get() << std::endl;
  std::cout << "graph->1->0: " << graph->at(1)->at(0)->get() << std::endl;
  std::cout << "graph->2->2: " << graph->at(2)->at(0)->get() << std::endl;
  std::cout << "graph->0->2->2: " << graph->at(0)->at(0)->at(0)->get() << std::endl;
  std::cout << "graph->1->0->2: " << graph->at(1)->at(0)->at(0)->get() << std::endl;

  std::cout << std::endl;
}
