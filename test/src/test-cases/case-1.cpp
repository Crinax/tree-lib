#include <iostream>
#include <memory>
#include <graph/node.hpp>
#include "case-1.h"

using namespace graph;

void test::case_1() {
  std::cout << "--> Test cyclic references(1) <--" << std::endl;
  std::shared_ptr<Node<std::size_t>> root =
    Node<std::size_t>::shared_from_value(12);

  root->link(root);
  
  std::cout << "First node: " << root->get() << std::endl;
  std::cout << "Cyclic connection: " <<
    root->at(0)->at(0)->get() << std::endl;

  std::cout << std::endl << "--> Trying to change :D" << std::endl;
  
  root->at(0)->at(0)->set(13);

  std::cout << "First node: " << root->get() << std::endl;
  std::cout << "Cyclic connection: " <<
    root->at(0)->at(0)->get() << std::endl; 
  std::cout << std::endl;
}
