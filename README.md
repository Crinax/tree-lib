# GraphL

Simple graph library

## Including to your project

```cpp
#include <graph/graph.hpp> // for graph class
#include <graph/node.hpp>  // for node class
```

## Testing

For test run building library with argument

```sh
./build.sh test
```
