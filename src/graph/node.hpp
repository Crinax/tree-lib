#pragma once

#include <vector>
#include <memory>

namespace graph {
  template<typename T>
  class Node {
    private:
      Node(T value) {
        this->_value = value;
        this->_connections = {};
      }

      std::vector<std::weak_ptr<Node<T>>> _connections;
      T _value;

    public:
      static Node<T>* from_value(T value) {
        return new Node<T>(value);
      }

      static std::shared_ptr<Node<T>> shared_from_value(T value) {
        return std::shared_ptr<Node<T>>(
          Node<T>::from_value(value)
        );
      }

      void link(std::shared_ptr<Node<T>> node) {
        this->_connections.push_back(node);
      }

      T get() {
        return this->_value;
      }

      void set(T value) {
        this->_value = value;
      }

      std::shared_ptr<Node<T>> at(std::size_t index) {
        return this->_connections.at(index).lock();
      }
  };
}
