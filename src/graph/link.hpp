#pragma once

#include <memory>

namespace graph {
  template<typename T = std::nullptr_t, typename Y = std::nullptr_t>
  class Link {
    private:
      Link() {};
      
      Link(std::shared_ptr<T> from) {
        this->_from = from;
      }

      Link(std::shared_ptr<T> from, std::shared_ptr<Y> to) {
        this->_from = from;
        this->_to = to;
      }

      std::shared_ptr<T> _from;
      std::shared_ptr<Y> _to;

    public:
      static Link<T, Y>* in_out(
        std::shared_ptr<T> from,
        std::shared_ptr<Y> to
      ) {
        return new Link<T, Y>(from, to);
      }

      static Link<T>* in(std::shared_ptr<T> from) {
        return new Link<T>(from);
      }

      static Link* empty() {
        return new Link();
      }

      std::shared_ptr<T> from() {
        return this->_from;
      }

      std::shared_ptr<Y> to() {
        return this->_to;
      }
  };
}
