#pragma once

#include <vector>
#include <memory>
#include "node.hpp"

namespace graph {
  template<typename T>
  class Graph {
    private:
      Graph() {}

      Graph(std::shared_ptr<Node<T>> value) {
        this->_nodes = { value };
      };

      std::vector<std::shared_ptr<Node<T>>> _nodes;

    public:
      static Graph<T>* from_value(T value) {
        std::shared_ptr<Node<T>> node =
          Node<T>::shared_from_value(value);

        return new Graph<T>(node);
      }

      static Graph<T>* empty() {
        return new Graph<T>();
      }

      std::shared_ptr<Node<T>> at(std::size_t index) {
        return this->_nodes.at(index);
      }

      void link(std::shared_ptr<Node<T>> node) {
        this->_nodes.push_back(node);
      }

      std::size_t count_nodes() {
        return this->_nodes.size();
      }
  };
}

